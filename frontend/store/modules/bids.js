import * as types from '../mutation-types'
import * as api from '../api'
		// socket: new WebSocket("ws://" + window.location.host + "/chat/"),

export default {
	state: {
		success: false,
		busy: false,
		bids: [],
		page: {},
		personalBids: [],
		personalPage: {},
		categories:{},
		currentBidItem:{},
		lastBids: [],
		isConnected:false
	},
	mutations: {
		[types.GET_BIDS] (state) {
			state.busy = true
		},
		[types.GET_BIDS_SUCCESS] (state, response) {
			state.busy = false
			state.page = response.data.pages
			state.bids = response.data.data
		},
		[types.GET_BIDS_FAIL] (state, error) {
			state.busy = false
		},
		[types.GET_MY_BIDS] (state) {
			state.busy = true
		},
		[types.GET_MY_BIDS_SUCCESS] (state, response) {
			state.busy = false
			state.personalPage = response.data.pages
			state.personalBids = response.data.data
		},
		[types.GET_MY_BIDS_FAIL] (state, error) {
			state.busy = false
		},
		[types.CREATE_BID] (state) {
			state.busy = true
		},
		[types.CREATE_BID_SUCCESS] (state, response) {
			state.busy = false
		},
		[types.CREATE_BID_FAIL] (state, error) {
			state.busy = false
		},
		[types.GET_CATEGORIES] (state, response) {
			state.categories = response.data.data
		},
		[types.PLACE_BID_SUCCESS] (state, response) {
			state.busy = false
			
		},
		[types.PLACE_BID_FAIL] (state, error) {
			state.busy = false
		},
		[types.CHANGE_BID_ITEM] (state, bidItem) {			
			Object.keys(state.bids).forEach(function(key) {
				if(state.bids[key].pk == bidItem){
					state.currentBidItem = state.bids[key]
				}
			});
		},
		[types.PLACE_BID] (state, response) {
			state.busy = true
		},
		[types.CHANGE_BID_ITEM_AMOUNT] (state, data) {		
			var dataPrased = JSON.parse(data)
			state.currentBidItem.current = dataPrased.amount
			state.currentBidItem.sold = dataPrased.sold
		},
		[types.GET_BID_ITEM] (state) {
			state.busy = true
		},
		[types.GET_BID_ITEM_SUCCESS] (state, response) {
			state.busy = false
			state.lastBids = response.data.data.lastBids
			state.currentBidItem = response.data.data.bid
		},
		[types.GET_BID_ITEM_FAIL] (state, error) {
			state.busy = false
		},
		[types.UPDATE_BID] (state) {
			state.busy = true
		},
		[types.UPDATE_BID_SUCCESS] (state, response) {
			state.busy = false
		},
		[types.UPDATE_BID_FAIL] (state, error) {
			state.busy = false
		},
	},
	actions: {
		getHomeBids: (store, {page, categories, priceFrom, priceTo}) => {
			store.commit(types.GET_BIDS)
			return api.getHomeBids(page, categories, priceFrom, priceTo)
						.then((response) => store.commit(types.GET_BIDS_SUCCESS, response),
								(error) => store.commit(types.GET_BIDS_FAIL, error))
		},
		getMyBids: (store, {page, categories, priceFrom, priceTo}) => {
			store.commit(types.GET_MY_BIDS)
			return api.getMyBidItems(page, categories, priceFrom, priceTo)
						.then((response) => store.commit(types.GET_MY_BIDS_SUCCESS, response),
								(error) => store.commit(types.GET_MY_BIDS_FAIL, error))
		},
		getMyWinnings: (store, {page, categories, priceFrom, priceTo}) => {
			store.commit(types.GET_MY_BIDS)
			return api.getMyWinnings(page, categories, priceFrom, priceTo)
						.then((response) => store.commit(types.GET_MY_BIDS_SUCCESS, response),
								(error) => store.commit(types.GET_MY_BIDS_FAIL, error))
		},
		getCategories: (store) => {
			return api.getCategories()
						.then((response) => store.commit(types.GET_CATEGORIES, response))
		},
		addBidItem: (store, {name, description, goal, tags, category, image}) => {
			store.commit(types.CREATE_BID)
			return api.createBid(name, description, goal, tags, category, image)
						.then((response) => store.commit(types.CREATE_BID_SUCCESS, response),
								(error) => store.commit(types.CREATE_BID_FAIL, error))
		},
		updateBidItem: (store, {name, description, tags, category, image, bidItem}) => {
			store.commit(types.UPDATE_BID)
			return api.updateBid(name, description, tags, category, image, bidItem)
						.then((response) => store.commit(types.UPDATE_BID_SUCCESS, response),
								(error) => store.commit(types.UPDATE_BID_FAIL, error))
		},
		placeBid: (store, {amount, bidItem}) => {
			store.commit(types.PLACE_BID)
			return api.placeBid(amount, bidItem)
						.then((response) => store.commit(types.PLACE_BID_SUCCESS, response),
								(error) => store.commit(types.PLACE_BID_FAIL, error))
		},
		setCurrentBid: (store, bidItem) => {
			store.commit(types.CHANGE_BID_ITEM, bidItem)
		},
		changeCurrentBidAmount: (store, data) => {
			store.commit(types.CHANGE_BID_ITEM_AMOUNT, data)
		},
		sell: (store, bidItem) => {
			return api.sell(bidItem)
		},
		getBidItem: (store, bidItem) => {
			store.commit(types.GET_BID_ITEM)
			return api.getBidItem(bidItem)
						.then((response) => store.commit(types.GET_BID_ITEM_SUCCESS, response),
								(error) => store.commit(types.GET_BID_ITEM_FAIL, error))
		},
		
	},
	getters: {
		homeBidItems: state => {
			return Object.keys(state.bids).length === 0 ? null : state.bids
		},
		myBidItems: state => {
			return Object.keys(state.personalBids).length === 0 ? null : state.personalBids
		},
		addBidCategory: state =>{
			var newArray = []
			Object.keys(state.categories).forEach(function(key) {
				var newObject = {}
				newObject.text = state.categories[key].name
				newObject.value = state.categories[key].name
				newArray.push(newObject)
			});
			return newArray
		},
		selectedBid: state => {
			return state.currentBidItem
		},
		homeBidPages: state => {
			return state.page
		},
		myBidPages: state => {
			return state.personalPage
		},
		homeCategories: state => {
			return state.categories
		},
		bidItemsBusy: state => {
			return state.busy
		},
		lastBids: state => {
			return state.lastBids
		},
	}
}
