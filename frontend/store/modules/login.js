import * as types from '../mutation-types'
import * as api from '../api'
export default {
	state: {
		success: false,
		busy: false,
		fail: false,
		registerErrors: {},
		loggedIn:false,
		user:{}
	},
	mutations: {
		[types.LOGIN_USER] (state) {
			state.busy = true
		},
		[types.LOGIN_USER_SUCCESS] (state, response) {
			state.busy = false
			state.loggedIn = true
			state.user = response.data.data
			location.reload()
		},
		[types.LOGIN_USER_FAIL] (state, error) {
			state.busy = false
			return error
		},
		[types.IS_LOGEGD_IN] (state) {
			state.busy = true
		},
		[types.IS_LOGEGD_IN_SUCCESS] (state, response) {
			state.busy = false
			state.loggedIn = true
			state.user = response.data.data		
		},
		[types.IS_LOGEGD_IN_FAIL] (state, error) {
			state.busy = false
		},
		[types.CREATE_USER] (state) {
			state.busy = true
		},
		[types.CREATE_USER_SUCCESS] (state, response) {
			state.busy = false
		},
		[types.CREATE_USER_FAIL] (state, error) {
			state.busy = false
			state.registerErrors = error.body.errors
			console.log(error)
		},
		[types.LOGOUT] (state) {
			state.busy = true
		},
		[types.LOGOUT_SUCCESS] (state, response) {
			state.busy = false
			state.user={}
			state.loggedIn=false
		},
		[types.LOGOUT_FAIL] (state, error) {
			state.busy = false
			state.registerErrors = error.body.errors
			console.log(error)
		}
	},
	actions: {
		login: (store, {username, password}) => {
			store.commit(types.LOGIN_USER)
			return api.login(username,password)
						.then((response) => store.commit(types.LOGIN_USER_SUCCESS, response),
								(error) => store.commit(types.LOGIN_USER_FAIL, error))
		},
		logout: (store) => {
			store.commit(types.LOGOUT)
			return api.logout()
						.then((response) => store.commit(types.LOGOUT_SUCCESS, response),
								(error) => store.commit(types.LOGOUT_FAIL, error))
		},
		isLoggedIn: (store) => {
			store.commit(types.IS_LOGEGD_IN)
			return api.isLoggedIn()
						.then((response) => store.commit(types.IS_LOGEGD_IN_SUCCESS, response),
								(error) => store.commit(types.IS_LOGEGD_IN_FAIL, error))
		},
		register: (store, {username, password, email}) => {
			store.commit(types.CREATE_USER)
			return api.register(username, password, email)
						.then((response) => store.commit(types.CREATE_USER_SUCCESS, response),
								(error) => store.commit(types.CREATE_USER_FAIL, error))
		},
	},
	getters: {
		loggedIn: state => {
			return state.loggedIn
		},
		currentUser: state => {
			return state.user
		},
		loginBusy: state => {
			return state.busy
		},
		// userGroup: state=>{
		// 	return false
		// }
	}
}
