import Vue from 'vue'
import VueResource from 'vue-resource'
Vue.use(VueResource)
function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}
Vue.http.headers.common['Access-Control-Allow-Origin'] = '*'

Vue.http.headers.common['X-CSRFToken'] = getCookie("csrftoken")
Vue.http.options.emulateHTTP = true;
// if (process.env.APP_DEBUG === 'true') {
	// Vue.http.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'
// }
// Vue.http.options.root = process.env.APP_URL

// Login user
export function login(username, password) {
	var data ={
		username: username,
		password: password,
	}
	return Vue.http.post('/login/', data, {
			emulateJSON: true
			}).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))
}

export function logout() {
	return Vue.http.post('/logout/', 
		).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))
}

export function isLoggedIn() {
	return Vue.http.post('/isLoggedIn/', 
		).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))
}

export function getHomeBids(page, categories, priceFrom, priceTo) {
	return Vue.http.get('/bids',{params:  {page: page, categories:categories,priceFrom:priceFrom,priceTo:priceTo}}, {
    	emulateJSON: true
		}).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))
}

export function getCategories() {
	return Vue.http.get('/categories', {
    	emulateJSON: true
		}).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))
}

// register user
export function register(username, password, email) {
	var data ={
		username: username,
		password: password,
		email: email,
	}
	return Vue.http.post('/register/', data, {
    	emulateJSON: true
		}).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))
}

// places bid
export function placeBid(amount, bidItem) {
	var data ={
		amount: amount,
		bidItem: bidItem,
	}
	return Vue.http.post('/placeBid/', data, {
			emulateJSON: true
			}).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))
}

// creates bidItem
export function createBid(name, description, goal, tags, category, image) {

	var formData = new FormData();
	formData.append('name', name);
	formData.append('description', description);
	formData.append('goal', goal);
	formData.append('tags', tags);
	formData.append('category', category);
	formData.append('image', image, image.name);


	return Vue.http.post('/createBid/', formData, {
	headers: {
		'Content-Type': 'multipart/form-data',
		'Accept': 'Accept:application/json'
	}
	}).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))


}

// updates bidItem
export function updateBid(name, description, tags, category, image, bidItem) {

	var formData = new FormData();
	formData.append('name', name);
	formData.append('description', description);
	formData.append('tags', tags);
	formData.append('category', category);
	formData.append('bid', bidItem);
	formData.append('image', image, image.name);


	return Vue.http.post('/updateBid/', formData, {
	headers: {
		'Content-Type': 'multipart/form-data',
		'Accept': 'Accept:application/json'
	}
	}).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))


}

// places bid
export function getMyBidItems(page, categories, priceFrom, priceTo) {
	return Vue.http.get('/myBidItems',{params:  {page: page, categories:categories,priceFrom:priceFrom,priceTo:priceTo}}, {
    	emulateJSON: true
		}).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))
}

// gets single item detailed
export function getBidItem(bidItem) {
	return Vue.http.get('/getBid',{params:  {bidId: bidItem}}, {
    	emulateJSON: true
		}).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))
}

// places bid
export function getMyWinnings(page, categories, priceFrom, priceTo) {
	return Vue.http.get('/myWinnings',{params:  {page: page, categories:categories,priceFrom:priceFrom,priceTo:priceTo}}, {
    	emulateJSON: true
		}).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))
}

// sells item to last buyer(highest bidder)
export function sell(bidItem) {
	var data ={
		bidId: bidItem
	}
	return Vue.http.post('/sell/', data, {
    	emulateJSON: true
		}).then((response) => Promise.resolve(response),
				(error) => Promise.reject(error))
}