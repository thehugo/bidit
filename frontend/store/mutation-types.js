// login
export const LOGIN_USER = 'LOGIN_USER'
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS'
export const LOGIN_USER_FAIL = 'LOGIN_USER_FAIL'

// logout
export const LOGOUT = 'LOGOUT'
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
export const LOGOUT_FAIL = 'LOGOUT_FAIL'

// check if logged in
export const IS_LOGEGD_IN = 'IS_LOGEGD_IN'
export const IS_LOGEGD_IN_SUCCESS = 'IS_LOGEGD_IN_SUCCESS'
export const IS_LOGEGD_IN_FAIL = 'IS_LOGEGD_IN_FAIL'

// register
export const CREATE_USER = 'CREATE_USER'
export const CREATE_USER_SUCCESS = 'CREATE_USER_SUCCESS'
export const CREATE_USER_FAIL = 'CREATE_USER_FAIL'

// get home Bids
export const GET_BIDS = 'GET_BIDS'
export const GET_BIDS_SUCCESS = 'GET_BIDS_SUCCESS'
export const GET_BIDS_FAIL = 'GET_BIDS_FAIL'

// place bid
export const PLACE_BID = 'PLACE_BID'
export const PLACE_BID_SUCCESS = 'PLACE_BID_SUCCESS'
export const PLACE_BID_FAIL = 'PLACE_BID_FAIL'

// create bid
export const CREATE_BID = 'CREATE_BID'
export const CREATE_BID_SUCCESS = 'CREATE_BID_SUCCESS'
export const CREATE_BID_FAIL = 'CREATE_BID_FAIL'

export const GET_CATEGORIES = 'GET_CATEGORIES'

export const CHANGE_BID_ITEM = 'CHANGE_BID_ITEM'
export const CHANGE_BID_ITEM_AMOUNT = 'CHANGE_BID_ITEM_AMOUNT'

// get home Bids
export const GET_MY_BIDS = 'GET_MY_BIDS'
export const GET_MY_BIDS_SUCCESS = 'GET_MY_BIDS_SUCCESS'
export const GET_MY_BIDS_FAIL = 'GET_MY_BIDS_FAIL'

// get Bid item and last bids
export const GET_BID_ITEM = 'GET_BID_ITEM'
export const GET_BID_ITEM_SUCCESS = 'GET_BID_ITEM_SUCCESS'
export const GET_BID_ITEM_FAIL = 'GET_BID_ITEM_FAIL'

// update Bid item
export const UPDATE_BID = 'UPDATE_BID'
export const UPDATE_BID_SUCCESS = 'UPDATE_BID_SUCCESS'
export const UPDATE_BID_FAIL = 'UPDATE_BID_FAIL'