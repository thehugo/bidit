import Vue from 'vue'
import Vuex from 'vuex'
import * as api from './api'
import * as types from './mutation-types'

import login from './modules/login'
import bids from './modules/bids'

Vue.use(Vuex)


export default new Vuex.Store({
	state: {

	},
	mutations:{

	},
	actions: {

	},
	getters: {

	},
	modules: {
		login,
		bids
	},
	strict: process.env.NODE_ENV !== 'production',
	plugins: []
})
