import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '../components/Views/Login.vue'
import Home from '../components/Views/Home.vue'
import AddBid from '../components/Views/AddBid.vue'
import Auction from '../components/Views/Auction.vue'
import MyBids from '../components/Views/MyBids.vue'
import EditBid from '../components/Views/EditBid.vue'
import MyWinnings from '../components/Views/MyWinnings.vue'

Vue.use(VueRouter)

export default new VueRouter({
	// hashbang: false,
	history: true,
	linkActiveClass: 'active',
	linkExactActiveClass: 'active-exact',
	routes: [
		{
			name: 'Index',
			path: '/',
			redirect: '/Home' // Dashboard.name
		},
		{
			name: Login.name,
			path: '/Login',
			component: Login
		},
		{
			name: Home.name,
			path: '/Home',
			component: Home
		},
		{
			name: AddBid.name,
			path: '/AddBid',
			component: AddBid
		},
		{
			name: Auction.name,
			path: '/Auction/:id',
			component: Auction
		},
		{
			name: MyBids.name,
			path: '/MyBids',
			component: MyBids
		},
		{
			name: EditBid.name,
			path: '/EditBid/:id',
			component: EditBid
		},
		{
			name: MyWinnings.name,
			path: '/MyWinnings',
			component: MyWinnings
		},
	]
})
