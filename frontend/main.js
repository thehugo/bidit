
import Vue from 'vue'

import App from './App.vue'

import store from './store'
import router from './router'

import 'jquery'

import Vuetify from 'vuetify'
Vue.use(Vuetify)
require('./sass/app.scss')

import 'vuetify/dist/vuetify.min.css';
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);
window.dev = process.env.NODE_ENV === 'development'

Vue.config.debug = process.env.NODE_ENV === 'development'
Vue.config.silent = !process.env.NODE_ENV === 'development'
Vue.config.productionTip = !process.env.NODE_ENV === 'development'

import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const messages = {
  en: {
  	  home: 'Home',
      login: 'Login',
      logout: 'Logout',
      addlisting: 'Add Listing',
      mylistings: 'My Listings',
      mywinnings: 'My Winnings',
      language: 'Language'
  },
  lv: {
  	  home: 'Sākums',
      login: 'Pieslēgties',
      logout: 'Iziet',
      addlisting: 'Pievienot sludinājumu',
      mylistings: 'Mani sludinājumi',
      mywinnings: 'Mani pirkumi',
      language: 'Valoda'
  }
}

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: 'lv', // set locale
  messages, // set locale messages
})


// Create a Vue instance with `i18n` option
new Vue({ i18n }).$mount('#app')


var vm = new Vue({
	// name: 'vm',
	el: '#app',
	router,
	Vuetify,
	store,
	i18n,
	render: h => h(App)
	// template: '<App/>',
	// components: { App }
}).$mount('#app')

