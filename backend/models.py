from django.db import models
from django.contrib.auth.models import User
import os
from uuid import uuid4
from django.utils.deconstruct import deconstructible


@deconstructible
class UploadToPathAndRename(object):

	def __init__(self, path):
		self.sub_path = path

	def __call__(self, instance, filename):
		ext = filename.split('.')[-1]
		if instance.pk:
			filename = '{}.{}'.format(instance.pk, ext)
		else:
			filename = '{}.{}'.format(uuid4().hex, ext)
		return os.path.join(self.sub_path, filename)

class Category(models.Model):
	name = models.CharField(max_length=50)
	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Category'
		verbose_name_plural = 'Categories'

class Bids(models.Model):
	name = models.CharField(max_length=50)
	goal = models.FloatField()
	current = models.FloatField()
	description = models.CharField(max_length=500)
	tags = models.CharField(max_length=500)
	sold = models.BooleanField(default=False)
	category = models.ForeignKey(Category, on_delete=models.CASCADE,)
	user = models.ForeignKey(User,on_delete=models.DO_NOTHING)
	img = models.ImageField(max_length=255, upload_to=UploadToPathAndRename(os.path.join('images')))
	won_by = models.ForeignKey(User,on_delete=models.DO_NOTHING, related_name='%(class)s_requests_created', blank=True, null=True)
	def __str__(self):
			return self.name + " by " + self.user.username

	def save(self, *args, **kwargs):
	        self.goal = round(self.goal, 2)
	        self.current = round(self.current, 2)
	        super(Bids, self).save(*args, **kwargs)

	class Meta:
		verbose_name = 'Bid'
		verbose_name_plural = 'Bids'

class TopList(models.Model):
	name = models.ForeignKey(Bids,on_delete=models.DO_NOTHING)
	def __str__(self):
		return self.name
	class Meta:
		verbose_name = 'TopList'
		verbose_name_plural = 'TopList'

class LastBids(models.Model):
	user = models.ForeignKey(User,on_delete=models.DO_NOTHING)
	bid = models.ForeignKey(Bids,on_delete=models.DO_NOTHING)
	amount = models.FloatField()
	created_at = models.DateTimeField(auto_now_add=True)

	def save(self, *args, **kwargs):
	        self.amount = round(self.amount, 2)
	        super(LastBids, self).save(*args, **kwargs)

	class Meta:
		verbose_name = 'Last Bid'
		verbose_name_plural = 'Last Bids'