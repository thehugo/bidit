from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework.pagination import PageNumberPagination
from backend.models import Bids, Category, LastBids

class GroupSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Group
		fields = ('name',)

class UserSerializer(serializers.HyperlinkedModelSerializer):
	groups = GroupSerializer(many=True)
	class Meta:
		model = User
		fields = ('username', 'email', 'groups')


class BidsSerializer(serializers.HyperlinkedModelSerializer):
	category = serializers.CharField(source='category.name')
	won_by = serializers.CharField(source='won_by.username', allow_null=True)
	user = serializers.CharField(source='user.username')
	class Meta:
		model = Bids
		fields = ('name', 'user', 'goal', 'current', 'description','tags','sold','pk','img', 'category', 'won_by')

class CategorySerializer(serializers.HyperlinkedModelSerializer):
	value = serializers.SerializerMethodField('checked')

	def checked(self, foo):
		return False

	class Meta:
		model = Category
		fields = ('name','value')

class LastBidSerializer(serializers.HyperlinkedModelSerializer):
	username = serializers.CharField(source='user.username')
	email = serializers.CharField(source='user.email')
	date = serializers.DateTimeField(source='created_at',format='%Y.%m.%d %H:%M')
	class Meta:
		model = LastBids
		fields = ('amount', 'username', 'email', 'date')