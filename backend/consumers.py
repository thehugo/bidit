# In consumers.py
from channels import Group

# Connected to websocket.connect
def ws_add(message, bid_id):
    # Accept the connection
    message.reply_channel.send({"accept": True})
    # Add to the chat group
    Group("%s" % bid_id).add(message.reply_channel)


# Connected to websocket.receive
def ws_message(message):
    print(message, "654")
    Group("chat").send({
        "text": "[user] %s" % message.content['text'],
    })

# Connected to websocket.disconnect
def ws_disconnect(message):
    Group("chat").discard(message.reply_channel)