
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin
from backend.models import Bids, Category, TopList, LastBids

admin.site.register(Bids)
admin.site.register(LastBids)
admin.site.register(Category)
admin.site.register(TopList)

import backend.views

from django.conf.urls import url, include
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', backend.views.index),
    url(r'^login/', backend.views.loginUser),
    url(r'^logout/', backend.views.logoutUser),
    url(r'^isLoggedIn/', backend.views.isLoggedIn),
    url(r'^bids/', backend.views.bidList),
    url(r'^myBidItems/', backend.views.myBidItemsList),
    url(r'^myWinnings/', backend.views.myWinningList),
    url(r'^register/', backend.views.createUser),
    url(r'^categories/', backend.views.categoryList),
    url(r'^createBid/', backend.views.createBid),
    url(r'^placeBid/', backend.views.placeBid),
    url(r'^getBid/', backend.views.getMyBidItem),
    url(r'^updateBid/', backend.views.updateBid),
    url(r'^sell/', backend.views.sell),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns.append(url(r'^__debug__/', include(debug_toolbar.urls)))
    # if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)