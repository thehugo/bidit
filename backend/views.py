from django.shortcuts import render
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from django.core import serializers
from django.views.generic import View
from django.http import JsonResponse, HttpResponse
from backend.models import Bids, Category, LastBids
from backend.serializers import UserSerializer, BidsSerializer, CategorySerializer, LastBidSerializer
from rest_framework import serializers
from rest_framework.parsers import JSONParser
from django.shortcuts import get_object_or_404
from rest_framework.settings import api_settings
from rest_framework.request import Request
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.serializers.json import DjangoJSONEncoder
import random
from django.contrib.auth.models import User, Group
from django.contrib.auth import logout, authenticate, login
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from channels import Group
import json

# bids = Bids.objects.
# all()
# current = 0
# for bid in bids:
#     current = current+1
#     bid.current = round(random.uniform(100, 800),2)
#     bid.goal = round(random.randint(801,1500))
#     bid.user = User.objects.get(pk=3)
#     bid.description = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et"
#     bid.name = "Random car" + str(current)
#     bid.save()

def index(request):
    return render(request, 'index.html')

def checkGroup(user, groupName):
    return user.groups.filter(name=groupName).exists()


@api_view(['GET'])
@authentication_classes((SessionAuthentication, BasicAuthentication))
@permission_classes((IsAuthenticated,))
def test(request, format=None):
    content = {

        # 'user': serializers.serialize('json', [ request.user, ]),  # `django.contrib.auth.User` instance.
        'auth': request.user.username,  # None
    }
    print(request.user)
    print(request.auth)

    return Response(content)


def logoutUser(request):
    print(request.user)
    logout(request)
    return JsonResponse({'status':'true'}, status=200)

def isLoggedIn(request):
    if request.user.is_authenticated:
        user = User.objects.get(username=request.user.username)
        serializer = UserSerializer(user, many=False, context={'request':request})
        return JsonResponse({'status':'true','data':serializer.data}, status=200)
    else:
        return JsonResponse({'status':'false'}, status=400)

@api_view(['POST'])
def loginUser(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            user = get_object_or_404(User, username=user.username)
            serializer = UserSerializer(user, many=False, context={'request':request})
            return JsonResponse({'status':'true','data':serializer.data}, status=200)
        else:
            return JsonResponse({'status':'false','message':'user not active'}, status=400)
    else:
        return JsonResponse({'status':'false','message':'Incorrect credentials'}, status=400)



@api_view(['POST'])
def createUser(request):
    username = request.POST['username']
    email = request.POST['email']
    password = request.POST['password']
    print(username, email, password)
    try:
        User.objects.get(username=username)
        return JsonResponse({'status':'false','errors':{'rUsername': 'Username already Exists'}}, status=500)
    except User.DoesNotExist:
        group = Group.objects.get(name='buyer')
        user = User.objects.create_user(username, email, password, group)
        return JsonResponse({'status':'true'}, status=200)


def createBid(request):
    # Handle file upload

    if request.method == 'POST' and checkGroup(request.user, 'seller'):
        name = request.POST['name']
        goal = int(request.POST['goal'])
        print(goal+1)
        description = request.POST['description']
        category = request.POST['category']
        tags = request.POST['tags']
        img = request.FILES['image']
        print(category)
        user = User.objects.get(username=request.user.username)
        category = Category.objects.get(name=category)

        bid = Bids.objects.create(name=name,goal=goal,description=description,category=category,tags=tags,img=img,user=user, current=0)
        bid.save()

        return JsonResponse({'status':'true'}, status=200)

def updateBid(request):

    if request.method == 'POST' and checkGroup(request.user, 'seller'):
        name = request.POST['name']
        description = request.POST['description']
        category = request.POST['category']
        tags = request.POST['tags']
        img = request.FILES['image']
        bid = int(request.POST['bid'])

        user = User.objects.get(username=request.user.username)
        category = Category.objects.get(name=category)

        currentBid = Bids.objects.get(pk=bid, user=user)
        currentBid.name=name
        currentBid.description=description
        currentBid.category=category
        currentBid.tags=tags
        currentBid.img=img
        currentBid.save()

        return JsonResponse({'status':'true'}, status=200)


def bidList(request):
    page = request.GET.get('page')
    print(request.GET.get('priceFrom'), request.GET.get('priceTo'))
    fromPrice = int(request.GET.get('priceFrom'))
    toPrice = int(request.GET.get('priceTo'))

    categoriesa = request.GET.getlist('categories[]')

    if categoriesa:
        categories = Category.objects.all().filter(name__in=categoriesa)
    else:
        categories = Category.objects.all()

    print(categories)

    if(fromPrice != 0 or toPrice != 0):
        bids = Bids.objects.filter(category__in=categories, goal__range=[fromPrice,toPrice]).order_by('-pk')
    else:
        bids = Bids.objects.filter(category__in=categories).order_by('-pk')

    paginator = Paginator(bids, 12)

    try:
        bids = paginator.page(page)
    except PageNotAnInteger:
        page = 1
        bids = paginator.page(1)
    except EmptyPage:
        page = 1
        bids = paginator.page(paginator.num_pages)

    pages = {
        'current':page,
        'amount':paginator.num_pages
        }

    serializer = BidsSerializer(bids, many=True, context={'request':request})
    # print(serializer.data)
    return JsonResponse({'status':'true', 'data':serializer.data, 'pages': pages}, status=200)

def myBidItemsList(request):
    if checkGroup(request.user, 'seller'):
        user = User.objects.get(username=request.user.username)
        page = request.GET.get('page')
        fromPrice = int(request.GET.get('priceFrom'))
        toPrice = int(request.GET.get('priceTo'))

        categoriesa = request.GET.getlist('categories[]')

        if categoriesa:
            categories = Category.objects.all().filter(name__in=categoriesa)
        else:
            categories = Category.objects.all()

        print(categories)

        if(fromPrice != 0 or toPrice != 0):
            bids = Bids.objects.filter(category__in=categories, goal__range=[fromPrice,toPrice], user=user).order_by('-pk')
        else:
            bids = Bids.objects.filter(category__in=categories, user=user).order_by('-pk')

        paginator = Paginator(bids, 12)

        try:
            bids = paginator.page(page)
        except PageNotAnInteger:
            page = 1
            bids = paginator.page(1)
        except EmptyPage:
            page = 1
            bids = paginator.page(paginator.num_pages)

        pages = {
            'current':page,
            'amount':paginator.num_pages
            }

        serializer = BidsSerializer(bids, many=True, context={'request':request})
        # print(serializer.data)
        return JsonResponse({'status':'true', 'data':serializer.data, 'pages': pages}, status=200)

def myWinningList(request):
    if checkGroup(request.user, 'buyer'):
        user = User.objects.get(username=request.user.username)
        page = request.GET.get('page')
        fromPrice = int(request.GET.get('priceFrom'))
        toPrice = int(request.GET.get('priceTo'))

        categoriesa = request.GET.getlist('categories[]')

        if categoriesa:
            categories = Category.objects.all().filter(name__in=categoriesa)
        else:
            categories = Category.objects.all()

        print(categories)

        if(fromPrice != 0 or toPrice != 0):
            bids = Bids.objects.filter(category__in=categories, goal__range=[fromPrice,toPrice], won_by=user).order_by('-pk')
        else:
            bids = Bids.objects.filter(category__in=categories, won_by=user).order_by('-pk')

        paginator = Paginator(bids, 12)

        try:
            bids = paginator.page(page)
        except PageNotAnInteger:
            page = 1
            bids = paginator.page(1)
        except EmptyPage:
            page = 1
            bids = paginator.page(paginator.num_pages)

        pages = {
            'current':page,
            'amount':paginator.num_pages
            }

        serializer = BidsSerializer(bids, many=True, context={'request':request})
        # print(serializer.data)
        return JsonResponse({'status':'true', 'data':serializer.data, 'pages': pages}, status=200)

def categoryList(request):
    categories = Category.objects.all().order_by('-pk')
    serializer = CategorySerializer(categories, many=True, context={'request':request})
    return JsonResponse({'status':'true', 'data':serializer.data}, status=200)

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def placeBid(request):
    amount = int(request.POST['amount'])
    bidItem = int(request.POST['bidItem'])

    bid = Bids.objects.get(pk=bidItem)
    user = User.objects.get(username=request.user.username)
    print(bid.name)
    print(user.username)
    if(bid.user != user):
        if(bid.sold == False):
            if(bid.current < amount):
                newPlacedBid = LastBids.objects.create(user=user, amount=amount, bid=bid)
                newPlacedBid.save()
            
                bid.current = amount
                bid.save()

                # jsoned={'text':{'item': bidItem, 'amount': bid.current}}
                jsoned=json.dumps({'item': bidItem, 'amount': bid.current, 'sold': False})
                result = {}
                result['text'] = jsoned
                Group("%s" % bid.pk).send(content=result)

                return JsonResponse({'status':'true'}, status=200)
            else:
                return JsonResponse({'status':'false', 'message': 'Bid is too small'}, status=500)
        else:
            return JsonResponse({'status':'false', 'message': 'Item is already sold'}, status=500)

def getMyBidItem(request):
    if checkGroup(request.user, 'seller'):
        bidId = int(request.GET.get('bidId'))
        user = User.objects.get(username=request.user.username)

        bid = Bids.objects.get(user=user, pk=bidId)
        lastBids = LastBids.objects.filter(bid=bid).order_by('-pk')

        bidSerializer = BidsSerializer(bid, many=False, context={'request':request})
        lastBidsSerializer = LastBidSerializer(lastBids, many=True, context={'request':request})
        print(bidSerializer.data, lastBidsSerializer.data)
        return JsonResponse({'status':'true', 'data':{'bid':bidSerializer.data, 'lastBids':lastBidsSerializer.data}}, status=200)

@api_view(['POST'])
def sell(request):
    if checkGroup(request.user, 'seller'):
        bidId = int(request.POST['bidId'])
        user = User.objects.get(username=request.user.username)

        bid = Bids.objects.get(user=user, pk=bidId)
        lastBids = LastBids.objects.filter(bid=bid).order_by('-pk')[:1].get()

        bid.sold = True
        bid.won_by = lastBids.user
        bid.save()

        jsoned=json.dumps({'item': bid.pk, 'amount': bid.current, 'sold': True})
        result = {}
        result['text'] = jsoned
        Group("%s" % bid.pk).send(content=result)

        return JsonResponse({'status':'true'}, status=200)