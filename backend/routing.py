from channels.routing import route
from backend.consumers import ws_add, ws_message, ws_disconnect

channel_routing = [
	route("websocket.connect", ws_add, path=r"^/(?P<bid_id>[a-zA-Z0-9_]+)/$"),    
	route("websocket.receive", ws_message),
    route("websocket.disconnect", ws_disconnect),
]